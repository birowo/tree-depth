var arr = [{id:'a',upline:'0'}, {id:'b',upline:'a'}, {id:'c',upline:'a'}, {id:'cc',upline:'a'}, {id:'d',upline:'b'},
{id:'e',upline:'b'}, {id:'f',upline:'c'}, {id:'g',upline:'c'}, {id:'gg',upline:'cc'}, {id:'h',upline:'d'},
{id:'i',upline:'d'}, {id:'ii',upline:'gg'}, {id:'j',upline:'h'}, {id:'k',upline:'h'}, {id:'hh',upline:'ii'},
{id:'kk',upline:'hh'}]
console.log(arr)

function buildTree(obj, _obj){
  for(var key in obj){
    if(typeof(obj[key])=='object') {
      if(key == _obj.upline) {
        obj[key][_obj.id] = {}
        return
      }
      buildTree(obj[key], _obj)
    }
  }
}

var root = {}
root[arr[0].upline]={}
for(var i=0; i<arr.length; i++) {
  buildTree(root, arr[i])
}

console.log(JSON.stringify(root, null, 3))
