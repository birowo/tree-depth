var arr = [{id:'a',upline:'0'}, {id:'b',upline:'a'}, {id:'c',upline:'a'}, {id:'cc',upline:'a'}, {id:'d',upline:'b'},
{id:'e',upline:'b'}, {id:'f',upline:'c'}, {id:'g',upline:'c'}, {id:'gg',upline:'cc'}, {id:'h',upline:'d'},
{id:'i',upline:'d'}, {id:'ii',upline:'gg'}, {id:'j',upline:'h'}, {id:'k',upline:'h'}, {id:'hh',upline:'ii'},
{id:'kk',upline:'hh'}]
console.log(arr)
var stack = {}
for(var i=0; i<arr.length; i++) {
  stack[arr[i].id]=[arr[i].id].concat(stack[arr[i].upline]||arr[i].upline)
}
console.log('stack:', stack)
var depth = {}
for(var key in stack){
  for(var i=0; i<stack[key].length; i++){
    if((depth[stack[key][i]]||0) < (i+1)) depth[stack[key][i]] = (i+1);
  }
}
console.log('depth:', depth)
